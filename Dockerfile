###########################################################
# Dockerfile that builds a test
###########################################################
FROM debian:buster-slim AS base

LABEL maintainer="genercan092@gmail.com"
ARG PUID=1000

ENV USER steam
ENV HOME "/home/"
ENV HOMEDIR ${HOME}${USER}
ENV STEAMCMDDIR "${HOMEDIR}/steamcmd"

# Install, update & upgrade packages
RUN set -x \
	&& dpkg --add-architecture i386 \
	&& apt-get -qq update \
	&& apt-get install -qq -y --no-install-recommends --no-install-suggests \
		lib32stdc++6=8.3.0-6 \
		lib32gcc1=1:8.3.0-6 \
		wget=1.20.1-1.1 \
		ca-certificates=20200601~deb10u2 \
		telnet \
		libsdl2-2.0-0:i386=2.0.9+dfsg1-1 \
		curl \
		locales=2.28-10 \
	&& sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
	&& dpkg-reconfigure --frontend=noninteractive locales \
    # Create user for the server
	&& useradd -u "${PUID}" -m "${USER}" \
        && su "${USER}" -c \
        # This also creates the home directory we later need
        # Create Directory for SteamCMD
                "mkdir -p \"${STEAMCMDDIR}\" \
                # Download SteamCMD
                # Extract and delete archive
                && wget -qO- 'https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz' | tar xvzf - -C \"${STEAMCMDDIR}\" \
                #Ejecute steamcmd.sh and quit
                && \".${STEAMCMDDIR}/steamcmd.sh\" +quit \
                && mkdir -p \"${HOMEDIR}/.steam/sdk32\" \
                && ln -s \"${STEAMCMDDIR}/linux32/steamclient.so\" \"${HOMEDIR}/.steam/sdk32/steamclient.so\" \
		&& ln -s \"${STEAMCMDDIR}/linux32/steamcmd\" \"${STEAMCMDDIR}/linux32/steam\" \
		&& ln -s \"${STEAMCMDDIR}/steamcmd.sh\" \"${STEAMCMDDIR}/steam.sh\"" \
	&& ln -s "${STEAMCMDDIR}/linux32/steamclient.so" "/usr/lib/i386-linux-gnu/steamclient.so" \
	&& ln -s "${STEAMCMDDIR}/linux64/steamclient.so" "/usr/lib/x86_64-linux-gnu/steamclient.so" \
    # Clean TMP, apt-get cache and other stuff to make the image smaller
	&& apt-get remove --purge -y \
		wget \
	&& apt-get clean autoclean \
	&& apt-get autoremove -y \
	&& rm -rf /var/lib/apt/lists/* \
    && rm -rf "${STEAMCMDDIR}/package/*"

# Switch to user
USER ${USER}

WORKDIR ${STEAMCMDDIR}

VOLUME ${STEAMCMDDIR}